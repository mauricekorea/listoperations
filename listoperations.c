#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertByKey(struct Node **head, int key, int num);
void insertByValue(struct Node **head, int value, int num);
 
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

void printList(struct Node *head)
{
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}

void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void deleteByKey(struct Node **head, int key)
{
    int index = 0;
    struct Node *current = *head;
    struct Node *previous = NULL;
    while(current != NULL) {
        if(index == key) {
            if(previous == NULL) {
                *head = current->next;
            } else {
                previous->next = current->next;
            }
            free(current);
            return;
        }
        previous = current;
        current = current->next;
        index++;
    }
}

void deleteByValue(struct Node **head, int value)
{
    struct Node *current = *head;
    struct Node *previous = NULL;
    while(current != NULL) {
        if(current->number == value) {
            if(previous == NULL) {
                *head = current->next;
            } else {
                previous->next = current->next;
            }
            free(current);
            return;
        }
        previous = current;
        current = current->next;
    }
}

void insertByKey(struct Node **head, int key, int num)
{
    int index = 0;
    struct Node *current = *head;
    struct Node *previous = NULL;
    while(current != NULL) {
        if(index == key) {
            struct Node *newNode = createNode(num);
            if(previous == NULL) {
                newNode->next = *head;
                *head = newNode;
            } else {
                newNode->next = current;
                previous->next = newNode;
            }
            return;
        }
        previous = current;
        current = current->next;
        index++;
    }
    // If the key is not found, append to the end
    append(head, num);
}

void insertByValue(struct Node **head, int value, int num)
{
    struct Node *current = *head;
    struct Node *previous = NULL;
    while(current != NULL) {
        if(current->number == value) {
            struct Node *newNode = createNode(num);
            if(previous == NULL) {
                newNode->next = *head;
                *head = newNode;
            } else {
                newNode->next = current;
                previous->next = newNode;
            }
            return;
        }
        previous = current;
        current = current->next;
    }
    // If the value is not found, append to the end
    append(head, num);
}

int main()
{
    struct Node *head = NULL;
    append(&head, 20); // [20]
    append(&head, 30); // [20, 30]
    prepend(&head, 10); // [10, 20, 30]
    printList(head); // [10, 20, 30]

    append(&head, 50); // [10, 20, 30, 50]
    prepend(&head, 95); // [95, 10, 20, 30, 50]
    printList(head); // [95, 10, 20, 30, 50]

    int choice, data;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch(choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4: ;
                char option;
                printf("Do you want to delete by value or key (v, k): ");
                scanf(" %c", &option); // Added space before %c to consume the newline character
                switch(option) {
                    case 'v':
                        printf("Enter value to delete: ");
                        scanf("%d", &data);
                        deleteByValue(&head, data);
                        break;
                    case 'k':
                        printf("Enter key to delete: ");
                        scanf("%d", &data);
                        deleteByKey(&head, data);
                        break;
                    default:
                        printf("Invalid option. Please try again.\n");
                }
                break;
            case 5:
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");

        }
    }

    return 0;
}
